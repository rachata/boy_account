import 'dart:async';

import 'package:boy/app_theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class ProfileEdit extends StatefulWidget {
  @override
  _ProfileEditState createState() => _ProfileEditState();
}

class _ProfileEditState extends State<ProfileEdit> {
  final picker = ImagePicker();
  StreamController<String> controller = new StreamController<String>();

  @override
  void initState() {
    super.initState();

    controller.add("assets/images/man1.png");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          "โปรไฟล์",
          style: TextStyle(fontFamily: AppTheme.fontName),
        ),
        backgroundColor: AppTheme.BoyGold800,
      ),
      body: SafeArea(
          child: SingleChildScrollView(
              child: Column(children: [
        Padding(
            padding: EdgeInsets.only(top: 20.0),
            child: new Stack(fit: StackFit.loose, children: <Widget>[
              new Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  StreamBuilder(
                      stream: controller.stream,
                      builder: (context, snapshot) {
                        return Container(
                          width: 140.0,
                          height: 140.0,
                          decoration: new BoxDecoration(
                            shape: BoxShape.circle,
                          ),
                          child: ClipRRect(
                              borderRadius: BorderRadius.circular(8.0),
                              child: kIsWeb ? Image.network(snapshot.data , fit: BoxFit.cover) : Image.file(snapshot.data , fit: BoxFit.cover),
                                  ),
                        );
                      })
                ],
              ),
              Padding(
                  padding: EdgeInsets.only(top: 90.0, right: 100.0),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      GestureDetector(
                        onTap: () => {selectImage()},
                        child: new CircleAvatar(
                          backgroundColor: Colors.blueGrey.withOpacity(0.5),
                          radius: 25.0,
                          child: new Icon(
                            Icons.camera_alt,
                            color: Colors.white,
                          ),
                        ),
                      )
                    ],
                  )),
            ])),
      ]))),
    );
  }

  selectImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      controller.add(pickedFile.path);
    } else {}
  }
}
