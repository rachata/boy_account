import 'dart:io';

import 'package:boy/app_theme.dart';
import 'package:boy/global.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
class ProfileStaff extends StatefulWidget {
  @override
  _ProfileStaffState createState() => _ProfileStaffState();
}

class _ProfileStaffState extends State<ProfileStaff> {

  final picker = ImagePicker();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          "โปรไฟล์",
          style: TextStyle(fontFamily: AppTheme.fontName),
        ),
        backgroundColor: AppTheme.BoyGold800,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                color: Colors.white,
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        IconButton(
                            onPressed: () => {editProfile()},
                            icon: Icon(
                              Icons.edit,
                              color: Colors.blueGrey,
                              size: 30,
                            )),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8.0),
                              color: Colors.white),
                          width: kIsWeb ? 100 : MediaQuery.of(context).size.width * 0.35,
                          child: ClipRRect(
                              borderRadius: BorderRadius.circular(8.0),
                              child: Image.asset("assets/images/man.png",
                                  fit: BoxFit.cover)),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Align(
                      child: Expanded(
                          child: Text(
                        "รัชฏ สุรียะ",
                        style: TextStyle(
                            fontFamily: AppTheme.fontName,
                            fontSize: 25,
                            fontWeight: FontWeight.w500),
                      )),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width * 0.30,
                          child: Column(
                            children: [
                              Text(
                                "32",
                                style: TextStyle(
                                    color: Colors.black87,
                                    fontFamily: AppTheme.fontName,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 18),
                              ),
                              Text("งานทั้งหมด",
                                  style: TextStyle(
                                      color: AppTheme.BoyGold800,
                                      fontFamily: AppTheme.fontName,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 18))
                            ],
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.30,
                          child: Column(
                            children: [
                              Text("32",
                                  style: TextStyle(
                                      color: Colors.black87,
                                      fontFamily: AppTheme.fontName,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 18)),
                              Text("งานที่เสร็จแล้ว",
                                  style: TextStyle(
                                      color: AppTheme.BoyGold800,
                                      fontFamily: AppTheme.fontName,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 18))
                            ],
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.30,
                          child: Column(
                            children: [
                              Text("32",
                                  style: TextStyle(
                                      color: Colors.black87,
                                      fontFamily: AppTheme.fontName,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 18)),
                              Text("งานที่ยังไม่เสร็จ",
                                  style: TextStyle(
                                      color: AppTheme.BoyGold800,
                                      fontFamily: AppTheme.fontName,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 18))
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(padding: EdgeInsets.only(top: 5)),
              Divider(
                color: Color(0xFFF4F5F7),
                thickness: 10,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                color: Colors.white,
                child: Padding(
                  child: Text(
                    "งานทั้งหมด",
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 20,
                        fontFamily: AppTheme.fontName),
                  ),
                  padding: EdgeInsets.all(10),
                ),
              ),
              Container(
                color: Colors.white,
                height: MediaQuery.of(context).size.width,
                width: MediaQuery.of(context).size.width,
                child: ListView.builder(
                  itemCount: friendsList.length,
                  itemBuilder: (ctx, i) {
                    return Column(
                      children: <Widget>[
                        ListTile(
                          isThreeLine: true,
                          onLongPress: () {},
//                          onTap: () => Navigator.of(context).push(
//                              MaterialPageRoute(
//                                  builder: (_) => DetailUserPage())),
                          leading: Container(
                            width: 50,
                            height: 50,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(
                                color: Colors.white,
                                width: 3,
                              ),
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.grey.withOpacity(.3),
                                    offset: Offset(0, 5),
                                    blurRadius: 25)
                              ],
                            ),
                            child: Stack(
                              children: <Widget>[
                                Positioned.fill(
                                  child: CircleAvatar(
                                    backgroundImage:
                                        NetworkImage(friendsList[i]['imgUrl']),
                                  ),
                                ),
                                friendsList[i]['isOnline']
                                    ? Align(
                                        alignment: Alignment.topRight,
                                        child: Container(
                                          height: 15,
                                          width: 15,
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                              color: Colors.white,
                                              width: 3,
                                            ),
                                            shape: BoxShape.circle,
                                            color: Colors.red,
                                          ),
                                        ),
                                      )
                                    : Container(),
                              ],
                            ),
                          ),
                          title: Text(
                            "${friendsList[i]['username']}",
                            style: Theme.of(context).textTheme.title,
                          ),
                          subtitle: Text(
                            "${friendsList[i]['lastMsg']}",
                            style: !friendsList[i]['seen']
                                ? Theme.of(context)
                                    .textTheme
                                    .subtitle
                                    .apply(color: Colors.black87)
                                : Theme.of(context)
                                    .textTheme
                                    .subtitle
                                    .apply(color: Colors.black54),
                          ),
                          trailing: Container(
                            width: 60,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
//                          friendsList[i]['seen']
//                              ? Icon(
//                                  Icons.check,
//                                  size: 15,
//                                )
//                              : Container(height: 15, width: 15),
                                    Text("${friendsList[i]['lastMsgTime']}")
                                  ],
                                ),
                                SizedBox(
                                  height: 5.0,
                                ),
                                friendsList[i]['hasUnSeenMsgs']
                                    ? Container(
                                        alignment: Alignment.center,
                                        height: 25,
                                        width: 25,
                                        decoration: BoxDecoration(
                                          color: Colors.red,
                                          shape: BoxShape.circle,
                                        ),
                                        child: Text(
                                          "${friendsList[i]['unseenCount']}",
                                          style: TextStyle(color: Colors.white),
                                        ),
                                      )
                                    : Container(
                                        height: 25,
                                        width: 25,
                                      ),
                              ],
                            ),
                          ),
                        ),
                        Divider()
                      ],
                    );
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  editProfile() async{

    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    if (pickedFile != null) {




    } else {

    }

  }
}
