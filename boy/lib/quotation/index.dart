import 'package:boy/app_theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';

class IndexQuotationPage extends StatefulWidget {
  IndexQuotationPage({Key key}) : super(key: key);

  @override
  IndexQuotationPageState createState() => IndexQuotationPageState();
}

class IndexQuotationPageState extends State<IndexQuotationPage> {
  final introKey = GlobalKey<IntroductionScreenState>();

  int typeCompany = 0;

  @override
  Widget build(BuildContext context) {
    const bodyStyle = TextStyle(fontSize: 19.0);
    const pageDecoration = const PageDecoration(
      titleTextStyle: TextStyle(fontSize: 28.0, fontWeight: FontWeight.w700),
      bodyTextStyle: bodyStyle,
      descriptionPadding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
      pageColor: Colors.white,
      imagePadding: EdgeInsets.zero,
    );

    return Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          child: IntroductionScreen(
            pages: [
              PageViewModel(
                image: Image.asset(
                  "assets/images/undraw.png",
                ),
                titleWidget: Text(
                  "ประเภทธุรกิจ",
                  style: TextStyle(
                      fontFamily: AppTheme.fontName,
                      fontSize: 22,
                      fontWeight: FontWeight.w700),
                ),
                bodyWidget: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Theme(
                          data: Theme.of(context).copyWith(
                              unselectedWidgetColor: Colors.black,
                              disabledColor: AppTheme.BoyGold800),
                          child: Radio(
                            value: 1,
                            groupValue: typeCompany,
                            onChanged: typeCompany == 1
                                ? null
                                : (int value) {
                                    setState(() {
                                      typeCompany = value;
                                    });
                                  },
                          ),
                        ),
                        Text(
                          "ธุรกิจการผลิต",
                          style: TextStyle(
                              color: typeCompany == 1
                                  ? AppTheme.BoyGold800
                                  : Colors.black,
                              fontFamily: AppTheme.fontName,
                              fontSize: 18,
                              fontWeight: typeCompany == 1
                                  ? FontWeight.w700
                                  : FontWeight.w400),
                        )
                      ],
                    ),
                    Row(
                      children: [
                        Theme(
                          data: Theme.of(context).copyWith(
                              unselectedWidgetColor: Colors.black,
                              disabledColor: AppTheme.BoyGold800),
                          child: Radio(
                            value: 2,
                            groupValue: typeCompany,
                            onChanged: typeCompany == 2
                                ? null
                                : (int value) {
                                    setState(() {
                                      typeCompany = value;
                                    });
                                  },
                          ),
                        ),
                        Text(
                          "ธุรกิจการค้าส่ง",
                          style: TextStyle(
                              color: typeCompany == 2
                                  ? AppTheme.BoyGold800
                                  : Colors.black,
                              fontFamily: AppTheme.fontName,
                              fontSize: 18,
                              fontWeight: typeCompany == 2
                                  ? FontWeight.w700
                                  : FontWeight.w400),
                        )
                      ],
                    ),
                    Row(
                      children: [
                        Theme(
                          data: Theme.of(context).copyWith(
                              unselectedWidgetColor: Colors.black,
                              disabledColor: AppTheme.BoyGold800),
                          child: Radio(
                            value: 3,
                            groupValue: typeCompany,
                            onChanged: typeCompany == 3
                                ? null
                                : (int value) {
                                    setState(() {
                                      typeCompany = value;
                                    });
                                  },
                          ),
                        ),
                        Text(
                          "ธุรกิจการค้าปลีก",
                          style: TextStyle(
                              color: typeCompany == 3
                                  ? AppTheme.BoyGold800
                                  : Colors.black,
                              fontFamily: AppTheme.fontName,
                              fontSize: 18,
                              fontWeight: typeCompany == 3
                                  ? FontWeight.w700
                                  : FontWeight.w400),
                        )
                      ],
                    ),
                    Row(
                      children: [
                        Theme(
                          data: Theme.of(context).copyWith(
                              unselectedWidgetColor: Colors.black,
                              disabledColor: AppTheme.BoyGold800),
                          child: Radio(
                            value: 4,
                            groupValue: typeCompany,
                            onChanged: typeCompany == 4
                                ? null
                                : (int value) {
                                    setState(() {
                                      typeCompany = value;
                                    });
                                  },
                          ),
                        ),
                        Text(
                          "ธุรกิจบริการ",
                          style: TextStyle(
                              color: typeCompany == 4
                                  ? AppTheme.BoyGold800
                                  : Colors.black,
                              fontFamily: AppTheme.fontName,
                              fontSize: 18,
                              fontWeight: typeCompany == 4
                                  ? FontWeight.w700
                                  : FontWeight.w400),
                        )
                      ],
                    )
                  ],
                ),
                decoration: pageDecoration,
              ),
              PageViewModel(
                image: Image.asset(
                  "assets/images/newspaper.png",
                ),
                titleWidget: Text(
                  "ธุรกิจของคุณเปิดในนามใด",
                  style: TextStyle(
                      fontFamily: AppTheme.fontName,
                      fontSize: 22,
                      fontWeight: FontWeight.w700),
                ),
                body:
                    "Instead of having to buy an entire share, invest any amount you want.",
                decoration: pageDecoration,
              ),
              PageViewModel(
                title: "Fractional shares",
                body:
                    "Instead of having to buy an entire share, invest any amount you want.",
                decoration: pageDecoration,
              ),
            ],
            onDone: () => _onIntroEnd(context),
            showSkipButton: false,
            skipFlex: 0,
            nextFlex: 0,
            done: const Text('ขอใบเสนอราคา',
                style: TextStyle(
                    color: AppTheme.BoyGold800,
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    fontFamily: AppTheme.fontName)),
            dotsDecorator: const DotsDecorator(
              activeColor: AppTheme.BoyGold800,
              size: Size(10.0, 10.0),
              color: AppTheme.BoyGold600,
              activeSize: Size(22.0, 10.0),
              activeShape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(25.0)),
              ),
            ),
          ),
        ));
  }

  void _onIntroEnd(context) {}
}
