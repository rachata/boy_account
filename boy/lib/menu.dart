import 'package:boy/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class MenuBar extends StatefulWidget {
  MenuBar();

  @override
  _MenuBarState createState() => _MenuBarState();
}

class _MenuBarState extends State<MenuBar> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppTheme.BoyGrey,
      body: Column(
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(top: 50),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Align(
                      alignment: Alignment.center,
                      child: Container(
                        margin:
                        const EdgeInsets.only(bottom: 5, top: 10, left: 5),
                        child: imageContainer("assets/images/LogoWithOutText.png"),
                      ),
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: Container(
                        margin:
                        const EdgeInsets.only(bottom: 10,left: 5),
                        child: Text("บอยการบัญชี" , style: TextStyle(color: Colors.white, fontFamily: AppTheme.fontName),),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                       Padding(padding: EdgeInsets.only(right: 10) , child:  Text("Version : 0.0.1" , style: TextStyle(color: Colors.white, fontSize: 12 , fontFamily: AppTheme.fontName),),)
                      ],
                    ),
                    const Divider(
                      thickness: 1.5,
                      color: AppTheme.BoyGold800,
                    ),
                    ...List.generate(1, (index) {
                      return ListTile(
                        leading: Icon(
                          Icons.home,
                          color: Colors.white,
                          size: 30,
                        ),
                        title: Text(
                          "หน้าแรก",
                          style: GoogleFonts.prompt(
                              fontSize: 18, color: Colors.white),
                        ),
                      );
                    }),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget imageContainer(String link) {
    return Image.asset(
      "${link}",
      width: 150,
      fit: BoxFit.cover,
    );
  }
}
