import 'package:boy/app_theme.dart';
import 'package:configurable_expansion_tile/configurable_expansion_tile.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ExpansionInfo extends StatefulWidget {
  final String title;
  final bool expand;
  final List<Widget> children;

  ExpansionInfo(
      {@required this.title, @required this.children, this.expand = true});

  @override
  State<StatefulWidget> createState() {
    return ExpansionInfoState();
  }
}

class ExpansionInfoState extends State<ExpansionInfo> {
  @override
  Widget build(BuildContext context) {
    return ConfigurableExpansionTile(
      initiallyExpanded: widget.expand,
      bottomBorderOn: false,
      topBorderOn: false,
      headerExpanded: Flexible(
        child: Container(
            color: AppTheme.BoyGold800,
            padding: const EdgeInsets.symmetric(vertical: 13.0, horizontal: 10),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Flexible(
                    child: Text("${widget.title.toUpperCase()}",
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: AppTheme.fontName,
                            fontSize: 18,
                            fontWeight: FontWeight.w700),
                        overflow: TextOverflow.ellipsis),
                  ),
                  const Spacer(),
                  Icon(
                    Icons.keyboard_arrow_up,
                    color: Colors.white,
                    size: 30,
                  ),
                ])),
      ),
      header: Flexible(
        child: Container(
            color: AppTheme.BoyGold800,
            margin: const EdgeInsets.only(bottom: 2),
            padding: const EdgeInsets.symmetric(vertical: 13.0, horizontal: 10),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Flexible(
                    child: Text("${widget.title.toUpperCase()}",
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: AppTheme.fontName,
                            fontSize: 18),
                        overflow: TextOverflow.ellipsis),
                  ),
                  Icon(
                    Icons.keyboard_arrow_right,
                    color: Colors.white,
                    size: 30,
                  )
                ])),
      ),
      children: widget.children,
    );
  }
}
