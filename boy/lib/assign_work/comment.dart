import 'package:boy/app_theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:footer/footer.dart';
import 'package:footer/footer_view.dart';

class CommentPage extends StatefulWidget {
  CommentPage({Key key}) : super(key: key);

  @override
  CommentPageState createState() => CommentPageState();
}

class CommentPageState extends State<CommentPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: AppTheme.BoyGold800,
        title: Text("แสดงความคิดเห็น",
            style: TextStyle(fontFamily: AppTheme.fontName)),
      ),
      body: SafeArea(
        child:

        FooterView(
            children: <Widget>[],
            footer: Footer(
              backgroundColor: Colors.white,
              child: Stack(
                children: <Widget>[



                  Align(
                    alignment: Alignment.bottomLeft,
                    child: Container(
                      padding: EdgeInsets.only(left: 10, bottom: 10, top: 10),
                      height: 80,
                      width: double.infinity,
                      color: Colors.white,
                      child: Row(
                        children: <Widget>[

                          GestureDetector(
                            onTap: () {},
                            child: Container(
                              height: 30,
                              width: 30,
                              decoration: BoxDecoration(
                                color: AppTheme.BoyGold800,
                                borderRadius: BorderRadius.circular(30),
                              ),
                              child: Icon(
                                Icons.add,
                                color: Colors.white,
                                size: 20,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 15,
                          ),
                          Expanded(
                            child: Container(
                              child: new ConstrainedBox(
                                constraints: BoxConstraints(
                                  maxHeight: 300.0,
                                ),
                                child: new Scrollbar(
                                  child: new SingleChildScrollView(
                                    scrollDirection: Axis.vertical,
                                    reverse: true,
                                    child:  Theme(
                                      data: Theme.of(context).copyWith(splashColor: Colors.transparent),
                                      child: TextField(
                                        maxLines: null,
                                        autofocus: false,
                                        style: TextStyle(fontFamily: AppTheme.fontName, fontSize: 18.0, color: Colors.black),
                                        decoration: InputDecoration(
                                          hintStyle: TextStyle(fontFamily: AppTheme.fontName, fontSize: 18.0, color: Colors.blueGrey),
                                          filled: true,
                                          fillColor: Colors.white,
                                          hintText: 'แสดงความคิดเห็น...',
                                          contentPadding:
                                          const EdgeInsets.only(left: 14.0, bottom: 8.0, top: 8.0),
                                          focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: AppTheme.BoyGold800),
                                            borderRadius: BorderRadius.circular(25.7),
                                          ),
                                          enabledBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(color: AppTheme.BoyGold800),
                                            borderRadius: BorderRadius.circular(25.7),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 15,
                          ),
                          FloatingActionButton(
                            onPressed: () {},
                            child: Icon(
                              Icons.send,
                              color: Colors.white,
                              size: 18,
                            ),
                            backgroundColor: AppTheme.BoyGold800,
                            elevation: 0,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            flex: 2),
      ),
    );
  }
}
