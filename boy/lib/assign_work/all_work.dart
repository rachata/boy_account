import 'package:boy/app_theme.dart';
import 'package:boy/assign_work/detail.dart';
import 'package:boy/provider/work.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:provider/provider.dart';

class AllWorkPage extends StatefulWidget {
  AllWorkPage({Key key}) : super(key: key);

  @override
  AllWorkPageState createState() => AllWorkPageState();
}

class AllWorkPageState extends State<AllWorkPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
        body: SafeArea(
            child: ListenableProvider.value(
      value: Provider.of<Work>(context, listen: false),
      child: Consumer<Work>(
        builder: (context, model, child) {
          if (model.workData.isEmpty) {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  kIsWeb == true
                      ? Image.asset(
                          "assets/images/empty.png",
                          height: 200,
                          width: size.width * 0.7,
                        )
                      : Image.asset(
                          "assets/images/empty.png",
                          width: size.width * 0.7,
                        ),
                  Text(
                    "ยังไม่มีข้อมูล ณ เวลานี้",
                    style:
                        TextStyle(fontSize: 18, fontFamily: AppTheme.fontName),
                  )
                ],
              ),
            );
          } else {
            return GestureDetector(
              child: Container(
                width: size.width,
                height: 200,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15.0),
                  boxShadow: [
                    const BoxShadow(
                      color: Colors.black26,
                      offset: Offset(0, 2),
                      blurRadius: 1,
                    )
                  ],
                ),
                margin: const EdgeInsets.only(
                  top: 15.0,
                  left: 15.0,
                  right: 15.0,
                  bottom: 10.0,
                ),
                child: Column(children: [
                  Expanded(
                      flex: 2,
                      child: Container(
                        padding: const EdgeInsets.only(
                            left: 10.0, top: 10.0, right: 15.0),
                        decoration: BoxDecoration(
                          borderRadius: const BorderRadius.only(
                            topLeft: Radius.circular(14.0),
                            bottomLeft: Radius.circular(14.0),
                            topRight: Radius.circular(14.0),
                            bottomRight: Radius.circular(14.0),
                          ),
                          color: Colors.white,
                        ),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Stack(
                                children: [
                                  const SizedBox(width: 92, height: 86),
                                  if (true)
                                    Positioned(
                                      right: 0,
                                      bottom: 0,
                                      child: Opacity(
                                        opacity: 0.6,
                                        child: Hero(
                                          tag: "image1",
                                          child: Container(
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(8.0),
                                                color: Colors.white),
                                            width: 80,
                                            height: 75,
                                            child: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(8.0),
                                                child: Image.asset(
                                                    "assets/images/man.png",
                                                    fit: BoxFit.cover)),
                                          ),
                                        ),
                                      ),
                                    ),
                                  Positioned(
                                    left: 0,
                                    top: 0,
                                    child: Hero(
                                      tag: "image2",
                                      child: Container(
                                        width: 85,
                                        height: 80,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                        ),
                                        child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                            child: Image.asset(
                                                "assets/images/man1.png",
                                                fit: BoxFit.cover)),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                              const SizedBox(width: 10),
                              Expanded(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    const SizedBox(height: 5),
                                    Wrap(
                                      children: [
                                        Flexible(
                                          child: RichText(
                                            overflow: TextOverflow.ellipsis,
                                            strutStyle:
                                                StrutStyle(fontSize: 12.0),
                                            text: TextSpan(
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontFamily:
                                                        AppTheme.fontName,
                                                    fontSize: 20),
                                                text: 'ของบการเงิน'),
                                          ),
                                        ),
                                      ],
                                    ),
                                    const SizedBox(height: 5),
                                    Text(
                                      'สถานะ : รับทราบงานแล้ว',
                                      style: TextStyle(
                                          fontFamily: AppTheme.fontName,
                                          fontSize: 16.0),
                                    ),
                                    const SizedBox(height: 5),
                                    Text(
                                      'ความสำคัญ : เร่งด่วน',
                                      style: TextStyle(
                                          fontFamily: AppTheme.fontName,
                                          fontSize: 16.0),
                                    ),
                                    const SizedBox(height: 5),
                                    Text(
                                      'ให้เสร็จ : 01/01/2564 12:00',
                                      style: TextStyle(
                                          fontFamily: AppTheme.fontName,
                                          fontSize: 16.0),
                                    ),
                                    const SizedBox(height: 4),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        LinearPercentIndicator(
                                          width: size.width * 0.5,
                                          lineHeight: 8.0,
                                          percent: 0.2,
                                          progressColor: Colors.green,
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ]),
                      ))
                ]),
              ),
              onTap: () => {
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (_) => DetailPage()))
              },
            );
          }
        },
      ),
    )));
  }
}
