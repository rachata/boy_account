import 'package:boy/ExpansionInfo.dart';
import 'package:boy/app_theme.dart';
import 'package:boy/assign_work/comment.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:super_tooltip/super_tooltip.dart';

class DetailPage extends StatefulWidget {
  DetailPage({Key key}) : super(key: key);

  @override
  DetailPageState createState() => DetailPageState();
}

class DetailPageState extends State<DetailPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: AppTheme.BoyGold800,
          title: Text("รายละเอียด",
              style: TextStyle(fontFamily: AppTheme.fontName)),
        ),
        body: SafeArea(
            child: SingleChildScrollView(
          child: Column(children: [
            Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.all(10),
//              decoration: BoxDecoration(
//                boxShadow: [
//                  BoxShadow(
//                    color: AppTheme.BoyGold800.withOpacity(0.2),
//                    spreadRadius: 5,
//                    blurRadius: 7,
//                    offset: Offset(0, 3), // changes position of shadow
//                  ),
//                ],
//                color: Colors.white,
//                borderRadius: BorderRadius.only(
//                    topLeft: Radius.circular(20.0),
//                    bottomLeft: Radius.circular(20.0),
//                    bottomRight: Radius.circular(20.0),
//                    topRight: Radius.circular(20.0)),
//              ),
              child: Column(
                children: [
                  Padding(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            "12/12/2012 23:59",
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontFamily: AppTheme.fontName,
                                fontSize: 14),
                          )
                        ],
                      )),
                  GestureDetector(
                    onTap: () => {callDetailUser()},
                    child: Padding(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8.0),
                                color: Colors.white),
                            width: 80,
                            child: ClipRRect(
                                borderRadius: BorderRadius.circular(8.0),
                                child: Image.asset("assets/images/man.png",
                                    fit: BoxFit.cover)),
                          ),
                          Padding(padding: EdgeInsets.only(right: 10)),
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8.0),
                                color: Colors.white),
                            width: 80,
                            child: ClipRRect(
                                borderRadius: BorderRadius.circular(8.0),
                                child: Image.asset("assets/images/man1.png",
                                    fit: BoxFit.cover)),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          LinearPercentIndicator(
                            width: size.width * 0.5,
                            lineHeight: 4.0,
                            percent: 0.2,
                            progressColor: Colors.green,
                          ),
                        ],
                      )),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.all(10),
//              decoration: BoxDecoration(
//                boxShadow: [
//                  BoxShadow(
//                    color: AppTheme.BoyGold800.withOpacity(0.2),
//                    spreadRadius: 5,
//                    blurRadius: 7,
//                    offset: Offset(0, 3), // changes position of shadow
//                  ),
//                ],
//                color: Colors.white,
//                borderRadius: BorderRadius.only(
//                    topLeft: Radius.circular(20.0),
//                    bottomLeft: Radius.circular(20.0),
//                    bottomRight: Radius.circular(20.0),
//                    topRight: Radius.circular(20.0)),
//              ),
              child: Column(
                children: [
                  Padding(
                      padding: const EdgeInsets.all(10),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width * 0.8,
                              child: Text(
                                "ของบการเงินบริษัท",
                                style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontFamily: AppTheme.fontName,
                                  fontSize: 20,
                                ),
                              ),
                            ),
                          ])),
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: Divider(
                      thickness: 2,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: 120,
                          child: Text(
                            "สถานะ :",
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontFamily: AppTheme.fontName,
                              fontSize: 16,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Text(
                            "รับทราบงานแล้ว",
                            style: TextStyle(
                              fontFamily: AppTheme.fontName,
                              fontSize: 16,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: 120,
                          child: Text(
                            "ความสำคัญ :",
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontFamily: AppTheme.fontName,
                              fontSize: 16,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Text(
                            "รับทราบงานแล้ว",
                            style: TextStyle(
                              fontFamily: AppTheme.fontName,
                              fontSize: 16,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: 120,
                          child: Text(
                            "ให้เสร็จ :",
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontFamily: AppTheme.fontName,
                              fontSize: 16,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Text(
                            "รับทราบงานแล้ว",
                            style: TextStyle(
                              fontFamily: AppTheme.fontName,
                              fontSize: 16,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            ExpansionInfo(title: "รายละเอียด", children: [
              Container(
                  color: Theme.of(context).cardColor,
                  padding:
                      const EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
                  child: Column(children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            child: Text(
                              "ของบการเงินบริษัท ก ตั้งแต่ 03/2563 - 03/2564 จากนั้นส่งเป็นไฟล์ให้ลูกค้าในกลุ่มพูดคุยงานได้เลย (ผ่านทางระบบใหม่) ",
                              style: TextStyle(
                                fontFamily: AppTheme.fontName,
                                fontSize: 18,
                                color: Colors.black,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ]))
            ]),
            Padding(
              padding: EdgeInsets.all(20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  GestureDetector(
                    onTap: () => {
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (_) => CommentPage()))

                    },
                    child: Container(
                      padding: EdgeInsets.all(10.0),
                      decoration: BoxDecoration(
                        border: Border.all(width: 1.0, color: Colors.blue),
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Text(
                        'แสดงความคิดเห็น',
                        style: TextStyle(
                            fontSize: 20,
                            color: Colors.blue,
                            fontFamily: AppTheme.fontName),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () => {},
                    child: Container(
                      padding: EdgeInsets.all(10.0),
                      decoration: BoxDecoration(
                        border: Border.all(width: 1.0, color: Colors.red),
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Text(
                        'อัพเดทงาน',
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                            fontSize: 20,
                            color: Colors.red,
                            fontFamily: AppTheme.fontName),
                      ),
                    ),
                  )
                ],
              ),
            )
          ]),
        )));
  }

  callDetailUser() {
    var t = SuperTooltip(
        borderColor: Colors.white,
        shadowColor: AppTheme.BoyGold800.withOpacity(0.5),
        popupDirection: TooltipDirection.up,
        top: MediaQuery.of(context).padding.top + 20,
        minWidth: MediaQuery.of(context).size.width,
        showCloseButton: ShowCloseButton.inside,
        closeButtonColor: Colors.red,
        arrowLength: 0,
        content: new Material(
            child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: SingleChildScrollView(
                  child: Container(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8.0),
                                  color: Colors.white),
                              width: 80,
                              child: ClipRRect(
                                  borderRadius: BorderRadius.circular(8.0),
                                  child: Image.asset("assets/images/man1.png",
                                      fit: BoxFit.cover)),
                            ),
                            Padding(padding: EdgeInsets.only(right: 10)),
                            Text(
                              "รัชฏ สุรียะ (ป้อม)",
                              style: TextStyle(
                                  fontSize: 20, fontFamily: AppTheme.fontName),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ))));

    t.show(context);
  }
}
