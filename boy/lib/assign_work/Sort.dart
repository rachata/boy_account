import 'package:boy/app_theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:smart_select/smart_select.dart';
import 'package:boy/c.dart' as choices;
class SortWidget extends StatefulWidget {
  @override
  SortWidgetState createState() => SortWidgetState();
}

class SortWidgetState extends State<SortWidget> {

  String _day = 'fri';

  @override
  Widget build(BuildContext context) {

    return SmartSelect<String>.single(

      modalHeaderStyle: S2ModalHeaderStyle(textStyle: TextStyle(fontFamily: AppTheme.fontName)),
      choiceHeaderStyle: S2ChoiceHeaderStyle(textStyle: TextStyle(fontSize: 20 , fontFamily: AppTheme.fontName)),
      title: 'ตัวเลือกการเรียง',

      value: _day,
      choiceItems: choices.days,
      onChange: (selected) => setState(() => _day = selected.value),
      modalType: S2ModalType.popupDialog,
      choiceType: S2ChoiceType.chips,
      choiceStyle: S2ChoiceStyle(
        titleStyle: TextStyle(fontFamily: AppTheme.fontName , fontSize:  16),
        activeColor: AppTheme.BoyGold800,

        color: Colors.blueGrey,
      ),


      tileBuilder: (context, state) {
        return IconButton(
          icon: Icon(FontAwesomeIcons.sort),
          onPressed: state.showModal,
        );
      },
    );
  }
}