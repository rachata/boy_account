import 'package:boy/app_theme.dart';
import 'package:boy/assign_work/Sort.dart';
import 'package:boy/assign_work/all_work.dart';
import 'package:boy/menu.dart';
import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_search_bar/flutter_search_bar.dart';

class IndexAssignWorkPage extends StatefulWidget {
  IndexAssignWorkPage({Key key}) : super(key: key);

  @override
  IndexAssignWorkPageState createState() => IndexAssignWorkPageState();
}

class IndexAssignWorkPageState extends State<IndexAssignWorkPage> {
  SearchBar searchBar;

  int selectPage = 0;

  final page = [
    AllWorkPage(),
    Container(),
    Container(),
    Container(),
    Container()
  ];

  @override
  void initState() {
    super.initState();

    searchBar = new SearchBar(
        hintText: "ค้นหา",
        inBar: false,
        setState: setState,
        onSubmitted: print,
        buildDefaultAppBar: buildAppBar);
  }

  AppBar buildAppBar(BuildContext context) {
    return new AppBar(
        backgroundColor: AppTheme.BoyGold800,
        title: new Text(
          'บอยการบัญชี',
          style: TextStyle(fontFamily: AppTheme.fontName),
        ),
        actions: [
          searchBar.getSearchAction(context),
          SortWidget(),
        ]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: searchBar.build(context),
        body: page[selectPage],
        drawer: Drawer(child: MenuBar()),
        backgroundColor: Colors.white,
        bottomNavigationBar: ConvexAppBar.badge(
          {0: "1", 1: "", 2: "", 3: "10+", 4: ""},
          backgroundColor: AppTheme.BoyGold800,


          color: Colors.white,
          style: TabStyle.flip,
          items: [
            TabItem(icon: Icons.home, title: 'งานทั้งหมด'),
            TabItem(icon: Icons.edit, title: 'ดำเนินการ'),
            TabItem(icon: Icons.add, title: 'สร้างงาน'),
            TabItem(icon: Icons.done, title: 'เสร็จแล้ว'),
            TabItem(icon: Icons.people, title: 'พนักงาน'),
          ],
          initialActiveIndex: 0,
          //optional, default as 0
          onTap: (int i) => {setPage(i)},
        ));
  }

  void setPage(int i) {
    setState(() {
      selectPage = i;
    });
  }
}
