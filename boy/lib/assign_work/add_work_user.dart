import 'package:badges/badges.dart';
import 'package:boy/app_theme.dart';
import 'package:boy/global.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:footer/footer.dart';
import 'package:footer/footer_view.dart';

class AddWorkUserPage extends StatefulWidget {
  @override
  _AddWorkUserPageState createState() => _AddWorkUserPageState();
}

class _AddWorkUserPageState extends State<AddWorkUserPage> {
  Map<String, bool> values = {};

  int checkListUser = 0;

  Future<bool> getMapUser() async {
    setState(() {
      checkListUser = friendsList.where((i) => i["check"]).toList().length;
    });

    return true;
  }

  void setMapUser(key, value) {
    setState(() {
      friendsList[key]["check"] = value;

      setState(() {
        checkListUser = friendsList.where((i) => i["check"]).toList().length;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: [
          Badge(
            badgeColor: Colors.red,
            position: BadgePosition.topEnd(top: 1, end: 1),
            badgeContent: Text("${checkListUser}" , style: TextStyle(fontSize: 16, fontFamily: AppTheme.fontName),),
            child: IconButton(
              iconSize: 30,
              icon: Icon(Icons.supervised_user_circle),
              onPressed: () {},
            ),
          ),
        ],
        backgroundColor: AppTheme.BoyGold800,
        title:
            Text("มอบหมายงาน", style: TextStyle(fontFamily: AppTheme.fontName)),
      ),
      body: SafeArea(
        child: FutureBuilder(
            future: getMapUser(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.hasData) {
                if (snapshot.data == true) {
                  return SingleChildScrollView(
                    child:
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          color: Colors.white,
                          height: MediaQuery.of(context).size.height * 0.7,
                          width: MediaQuery.of(context).size.width,
                          child: ListView.builder(
                            itemCount: friendsList.length,
                            itemBuilder: (ctx, i) {
                              return Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  ListTile(
                                    isThreeLine: true,
                                    onLongPress: () {},
                                    leading: Container(
                                      width: 50,
                                      height: 50,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        border: Border.all(
                                          color: Colors.white,
                                          width: 3,
                                        ),
                                        boxShadow: [
                                          BoxShadow(
                                              color:
                                                  Colors.grey.withOpacity(.3),
                                              offset: Offset(0, 5),
                                              blurRadius: 25)
                                        ],
                                      ),
                                      child: Stack(
                                        children: <Widget>[
                                          Positioned.fill(
                                            child: CircleAvatar(
                                              backgroundImage: NetworkImage(
                                                  friendsList[i]['imgUrl']),
                                            ),
                                          ),
                                          friendsList[i]['isOnline']
                                              ? Align(
                                                  alignment: Alignment.topRight,
                                                  child: friendsList[i]
                                                          ['hasUnSeenMsgs']
                                                      ? Container(
                                                          alignment:
                                                              Alignment.center,
                                                          height: 20,
                                                          width: 20,
                                                          decoration:
                                                              BoxDecoration(
                                                            color: Colors.red,
                                                            shape:
                                                                BoxShape.circle,
                                                          ),
                                                          child: Text(
                                                            "${friendsList[i]['unseenCount']}",
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white),
                                                          ),
                                                        )
                                                      : Container(
                                                          height: 25,
                                                          width: 25,
                                                        ),
                                                )
                                              : Container(),
                                        ],
                                      ),
                                    ),
                                    title: Text(
                                      "${friendsList[i]['username']}",
                                      style: Theme.of(context).textTheme.title,
                                    ),
                                    subtitle: Text(
                                      "${friendsList[i]['lastMsg']}",
                                      style: !friendsList[i]['seen']
                                          ? Theme.of(context)
                                              .textTheme
                                              .subtitle
                                              .apply(color: Colors.black87)
                                          : Theme.of(context)
                                              .textTheme
                                              .subtitle
                                              .apply(color: Colors.black54),
                                    ),
                                    trailing: Container(
                                      width: 60,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: <Widget>[
                                          SizedBox(
                                            height: 5.0,
                                          ),
                                          Checkbox(
                                            value: friendsList[i]['check'],
                                            activeColor: AppTheme.BoyGold800,
                                            onChanged: (bool value) {
                                              setMapUser(i, value);
                                            },
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Divider()
                                ],
                              );
                            },
                          ),
                        ),
                        RaisedButton.icon(
                          padding: EdgeInsets.all(15),
                          onPressed: () {
                            print('Button Clicked.');
                          },
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0))),
                          label: Text(
                            'สร้างงาน',
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: AppTheme.fontName,
                                fontSize: 18),
                          ),
                          icon: Icon(
                            Icons.add,
                            color: Colors.white,
                          ),
                          textColor: Colors.white,
                          splashColor: Colors.red,
                          color: AppTheme.BoyGold800,
                        ),
                      ],
                    ),
                  );
                } else {
                  return Text("ผิดพลากชด");
                }
              } else {
                return Text("ผิดพลากชด");
              }
            }),
      ),
    );
  }
}
