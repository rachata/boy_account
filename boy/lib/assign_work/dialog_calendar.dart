import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DialogCalendar extends StatefulWidget {
  @override
  _DialogCalendarState createState() => _DialogCalendarState();
}

class _DialogCalendarState extends State<DialogCalendar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: RaisedButton(
        child: Text("Submit"),
        onPressed: () {
          String text = "Data that we want to pass. Can be anything.";
          Navigator.pop(context, text);
        },
      )
      ,
    );
  }
}
