import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:boy/app_theme.dart';
import 'package:boy/assign_work/dialog_calendar.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mime/mime.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';
import 'package:syncfusion_flutter_core/theme.dart';

class AddWorkPage extends StatefulWidget {
  @override
  _AddWorkPageState createState() => _AddWorkPageState();
}

class _AddWorkPageState extends State<AddWorkPage> {
  final focus = FocusNode();
  dynamic _stepSliderValue = 1.0;

  bool deleteAll = false;

  StreamController<List<File>> controller = new StreamController<List<File>>();

  List<PlatformFile> _paths = [];
  List<File> files = [];

  void openPicker(bool status) async {
    final DateTime date = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1900),
        lastDate: DateTime(2200),
        builder: (BuildContext context, Widget child) {
          return Theme(
            data: ThemeData.dark().copyWith(
              colorScheme: ColorScheme.dark(primary: AppTheme.BoyGold800),
              buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.primary),
            ),
            child: child,
          );
        });

    TimeOfDay now = TimeOfDay.now();

    final TimeOfDay time = await showTimePicker(
        context: context,
        initialTime: TimeOfDay(hour: now.hour, minute: now.minute),
        builder: (BuildContext context, Widget child) {
          return Theme(
            data: ThemeData.dark().copyWith(
              colorScheme: ColorScheme.dark(primary: AppTheme.BoyGold800),
              buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.primary),
            ),
            child: child,
          );
        });
  }

  Future<String> getSize(File file, int decimals) async {
    int bytes = await file.length();

    if (bytes <= 0) return "0 B";
    const suffixes = ["B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
    var i = (log(bytes) / log(1024)).floor();
    return ((bytes / pow(1024, i)).toStringAsFixed(decimals)) +
        ' ' +
        suffixes[i];
  }

  void deleteFile(index) {
    files.removeAt(index);

    if (files.length >= 1) {
      setState(() {
        deleteAll = true;
      });
    }else{
      setState(() {
        deleteAll = false;
      });
    }

    controller.add(files);
  }

  void selectFile() async {
//    await FilePicker.platform.clearTemporaryFiles();

    try {
      FilePickerResult result = await FilePicker.platform
          .pickFiles(type: FileType.any, allowMultiple: true);

      print("result ${result}");

      if (result != null) {

        List<File> tem = result.paths.map((path) => File(path)).toList();

        files.addAll(tem);
        

        print("files ${files.length}");

        controller.add(files);

        if (files.length >= 1) {
          setState(() {
            deleteAll = true;
          });
        }else{
          setState(() {
            deleteAll = false;
          });
        }
      } else {
        if (files.length >= 1) {
          setState(() {
            deleteAll = true;
          });
        }else{
          setState(() {
            deleteAll = false;
          });
        }

        controller.add(files);
      }
    } on PlatformException catch (e) {
      print("Unsupported operation" + e.toString());
    } catch (ex) {
      print(ex);
    }
  }

  deleteFileAll() {
    files = [];

    if (files.length >= 1) {
      setState(() {
        deleteAll = true;
      });
    }else{
      setState(() {
        deleteAll = false;
      });
    }

    controller.add(files);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
//        actions: [
//          IconButton(icon: Icon(Icons.chat), onPressed: () => {selectFile()})
//        ],
        title: Text(
          "สร้างงาน",
          style: TextStyle(fontFamily: AppTheme.fontName),
        ),
        backgroundColor: AppTheme.BoyGold800,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Form(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          top: 15, bottom: 15, right: 15, left: 15),
                      child: Text(
                        "หัวข้องาน",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                            fontFamily: AppTheme.fontName),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 15, right: 15, left: 15),
                      child: Theme(
                        data: ThemeData(
                            primaryColor: Colors.black87,
                            accentColor: Colors.black87,
                            hintColor: Colors.black87 //This is Ignored,

                            ),
                        child: new TextFormField(
                          textInputAction: TextInputAction.next,
                          maxLines: null,
                          style: TextStyle(
                              fontSize: 18,
                              fontFamily: AppTheme.fontName,
                              color: Colors.black87,
                              decorationColor: Colors.black87),
                          cursorColor: Colors.black,
                          decoration: InputDecoration(
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppTheme.BoyGold800, width: 1.0),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppTheme.BoyGold800, width: 2.0),
                              ),
                              hintText: "หัวข้อ",
                              hintStyle: TextStyle(
                                  fontFamily: AppTheme.fontName,
                                  color: Colors.blueGrey,
                                  fontSize: 16)),
                        ),
                        //new Divider(color: Colors.white, height: 20),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 15, right: 15, left: 15),
                      child: Text(
                        "รายละเอียด",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                            fontFamily: AppTheme.fontName),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 15, right: 15, left: 15),
                      child: Theme(
                        data: ThemeData(
                            primaryColor: Colors.black87,
                            accentColor: Colors.black87,
                            hintColor: Colors.black87 //This is Ignored,

                            ),
                        child: new TextFormField(
                          textInputAction: TextInputAction.next,
                          maxLines: null,
                          style: TextStyle(
                              fontSize: 18,
                              fontFamily: AppTheme.fontName,
                              color: Colors.black87,
                              decorationColor: Colors.black87),
                          cursorColor: Colors.black,
                          decoration: InputDecoration(
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppTheme.BoyGold800, width: 1.0),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppTheme.BoyGold800, width: 2.0),
                              ),
                              hintText: "รายละเอียด",
                              hintStyle: TextStyle(
                                  fontFamily: AppTheme.fontName,
                                  color: Colors.blueGrey,
                                  fontSize: 16)),
                        ),
                        //new Divider(color: Colors.white, height: 20),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 15, right: 15, left: 15),
                      child: GestureDetector(
                        onTap: () => {openPicker(true)},
                        child: Row(
                          children: [
                            Icon(Icons.date_range),
                            Padding(padding: EdgeInsets.only(right: 10)),
                            Text(
                              "วัน/เวลา เริ่ม",
                              style: TextStyle(
                                  fontSize: 18, fontFamily: AppTheme.fontName),
                            )
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 15, right: 15, left: 15),
                      child: GestureDetector(
                        onTap: () => {openPicker(false)},
                        child: Row(
                          children: [
                            Icon(Icons.date_range),
                            Padding(padding: EdgeInsets.only(right: 10)),
                            Text(
                              "วัน/เวลา ส่งงาน",
                              style: TextStyle(
                                  fontSize: 18, fontFamily: AppTheme.fontName),
                            )
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 15, right: 15, left: 15),
                      child: Text(
                        "ความสำคัญ",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                            fontFamily: AppTheme.fontName),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 15, right: 15, left: 15),
                      child: SfSliderTheme(
                          data: SfSliderThemeData(
                              activeLabelStyle: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  color: AppTheme.BoyGold600,
                                  fontSize: 16,
                                  fontFamily: AppTheme.fontName),
                              inactiveLabelStyle: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  color: AppTheme.BoyGold600,
                                  fontSize: 16,
                                  fontFamily: AppTheme.fontName),
                              tooltipTextStyle:
                                  TextStyle(fontFamily: AppTheme.fontName)),
                          child: SfSlider(
                              activeColor: AppTheme.BoyGold800,
                              inactiveColor:
                                  AppTheme.BoyGold600.withOpacity(0.5),
                              labelFormatterCallback:
                                  (dynamic actualValue, String formattedText) {
                                if (actualValue == 1.0) {
                                  return "ต่ำ";
                                } else if (actualValue == 2.0) {
                                  return "ปานกลาง";
                                } else if (actualValue == 3.0) {
                                  return "เร่ง";
                                } else if (actualValue == 4.0) {
                                  return "เร่งด่วน";
                                }
                              },
                              tooltipTextFormatterCallback:
                                  (dynamic actualValue, String formattedText) {
                                if (actualValue == 1.0) {
                                  return "ต่ำ";
                                } else if (actualValue == 2.0) {
                                  return "ปานกลาง";
                                } else if (actualValue == 3.0) {
                                  return "เร่ง";
                                } else if (actualValue == 4.0) {
                                  return "เร่งด่วน";
                                }
                              },
                              showLabels: true,
                              interval: 1,
                              min: 1.0,
                              max: 4.0,
                              stepSize: 1.0,
                              showTicks: true,
                              value: _stepSliderValue,
                              onChanged: (dynamic values) {
                                setState(() {
                                  _stepSliderValue = values;
                                });
                              },
                              enableTooltip: true)),
                    )
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () => {selectFile()},
                      child: Row(
                        children: [
                          Text(
                            "แนบไฟล์",
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 18,
                                fontFamily: AppTheme.fontName),
                          ),
                          IconButton(
                              icon: Icon(
                            Icons.attach_file,
                            color: Colors.black87,
                            size: 25,
                          ))
                        ],
                      ),
                    ),
                    deleteAll == true
                        ? GestureDetector(
                            onTap: () => {deleteFileAll()},
                            child: Row(
                              children: [
                                Text(
                                  "ลบไฟล์ทั้งหมด",
                                  style: TextStyle(
                                      color: Colors.red,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 16,
                                      fontFamily: AppTheme.fontName),
                                ),
                                IconButton(
                                    icon: Icon(
                                  Icons.restore_from_trash,
                                  color: Colors.red,
                                  size: 20,
                                ))
                              ],
                            ),
                          )
                        : Container(),
                  ],
                ),
              ),
              Container(
                  color: Colors.white,
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.width,
                  child: StreamBuilder(
                    stream: controller.stream,
                    builder: (context, snapshot) {
                      return ListView.builder(
                          itemCount: files.length,
                          itemBuilder: (context, index) {
                            return FutureBuilder(
                                future: getSize(files[index], 1),
                                builder: (BuildContext context,
                                    AsyncSnapshot snapshot) {
                                  return Padding(
                                    padding: EdgeInsets.all(10),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Flexible(
                                              child: Text(
                                                " ${files[index].path.split("/").last} ${snapshot.data}",
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    fontFamily:
                                                        AppTheme.fontName),
                                              ),
                                            ),
                                            IconButton(
                                                icon: Icon(
                                                  Icons.remove_circle,
                                                  size: 30,
                                                  color: Colors.red,
                                                ),
                                                onPressed: () =>
                                                    {deleteFile(index)})
                                          ],
                                        ),
                                        lookupMimeType(files[index].path)
                                                    .split("/")
                                                    .first ==
                                                "image"
                                            ? Image.file(
                                                files[index],
                                                width: 50,
                                              )
                                            : Container()
                                      ],
                                    ),
                                  );
                                });
                          });
                    },
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
