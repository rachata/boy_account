import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class AppTheme {
  AppTheme._();

  static const String fontName = 'Prompt';

  static const Color Blue = Color(0xff38547d);
  static const Color BoyGrey = Color(0xFF3a3a3a);

  static const Color BoyGold800 = Color(0xffA57B2B);
  static const Color BoyGold600 = Color(0xffBD8B2C);
  static const Color BoyGold400 = Color(0xffD19929);
  static const Color BoyGold200 = Color(0xffF1B01C);
}
